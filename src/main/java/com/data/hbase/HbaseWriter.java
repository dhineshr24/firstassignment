package com.data.hbase;

import com.service.HBaseTableInterface;
import com.service.VariableGetter;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.util.Bytes;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class HbaseWriter implements HBaseTableInterface {


    VariableGetter variables = new VariableGetter();

    private String HADOOP_HOME = variables.getHADOOP_HOME();

    private TableName TABLE_NAME = variables.getTABLE_NAME();
    private byte[] CF_NAME = variables.getCF_NAME();


    private byte[] PERSON_NAME = variables.getPERSON_NAME();
    private byte[] PERSON_AGE = variables.getPERSON_AGE();
    private byte[] PERSON_COMPANY = variables.getPERSON_COMPANY();
    private byte[] PERSON_BUILDING_CODE = variables.getPERSON_BUILDING_CODE();
    private byte[] PERSON_PHONE_NUMBER = variables.getPERSON_PHONE_NUMBER();
    private byte[] PERSON_ADDRESS = variables.getPERSON_ADDRESS();


    @Override
    public void write(int noOfFiles) throws IOException {

        Configuration conf = new Configuration();

        FileSystem hdfs = FileSystem.get(conf);

        //TO ESTABLISH CONNECTION WITH HBASE and CREATE TABLE IF IT DOESN'T EXIST
        Connection connection = createTable();

        //UPLOADING DATA READ FROM HDFS TO Hbase TABLE
        for (int i = 0; i < noOfFiles; i++) {

            Path file = new Path("/user/DevHarsha/output/person_" + i + ".csv");
            FSDataInputStream is = hdfs.open(file);
            BufferedReader br = new BufferedReader(new InputStreamReader(is, "UTF-8"));

            br.readLine();
            String lineRead = br.readLine();
            while (lineRead != null) {
                String data[] = lineRead.split(",", 6);
                //Name, age, company, building_code, phone_number, address
                try (Table table = connection.getTable(TABLE_NAME)) {
                    table.put(new Put(Bytes.toBytes("row" + i)).addColumn(CF_NAME, PERSON_NAME, Bytes.toBytes(data[0])));
                    table.put(new Put(Bytes.toBytes("row" + i)).addColumn(CF_NAME, PERSON_AGE, Bytes.toBytes(data[1])));
                    table.put(new Put(Bytes.toBytes("row" + i)).addColumn(CF_NAME, PERSON_COMPANY, Bytes.toBytes(data[2])));
                    table.put(new Put(Bytes.toBytes("row" + i)).addColumn(CF_NAME, PERSON_BUILDING_CODE, Bytes.toBytes(data[3])));
                    table.put(new Put(Bytes.toBytes("row" + i)).addColumn(CF_NAME, PERSON_PHONE_NUMBER, Bytes.toBytes(data[4])));
                    table.put(new Put(Bytes.toBytes("row" + i)).addColumn(CF_NAME, PERSON_ADDRESS, Bytes.toBytes(data[5])));
                }

                lineRead = br.readLine();
            }
            br.close();
        }
        hdfs.close();

    }

    private Connection createTable() throws IOException {

        Configuration config = HBaseConfiguration.create();
        Connection connection = ConnectionFactory.createConnection(config);
        Admin admin = connection.getAdmin();

        if (!admin.tableExists(TABLE_NAME)) {
            TableDescriptor desc = TableDescriptorBuilder.newBuilder(TABLE_NAME)
                    .setColumnFamily(ColumnFamilyDescriptorBuilder.of(CF_NAME))
                    .build();
            admin.createTable(desc);
        }

        return connection;
    }
}
